$(document).ready(function () {

  svg4everybody();


  var promoThumbs = new Swiper('.s-promo-slider__slider .swiper-container', {
    centeredSlides: true,
    autoplay: {
      delay: 5000,
    },
    loop: true,
    watchSlidesVisibility: true,
    watchSlidesProgress: true,
    navigation: {
      nextEl: '.s-promo-slider__button-next',
      prevEl: '.s-promo-slider__button-prev',
    },
    breakpoints: {
      320: {
        slidesPerView: 1
      },
      576: {
        slidesPerView: 3
      },
      1200: {
        slidesPerView: 5
      }
    }
  });

  var promoTop = new Swiper('.s-promo__slider .swiper-container', {
    autoplay: {
      delay: 5000,
    },
    loop: true,
    thumbs: {
      swiper: promoThumbs
    }
  });




  $('.s-page-header__search-btn').on('click', function (e) {
    $('.s-page-header__search-form').toggleClass('s-page-header__search-form--is-open')
  });

  if ($('.s-main-nav__list').css('flex-direction') == 'column') {
    $('.s-main-nav__list > li > a').each(function () {
      var text = $(this).text()
      $(this).html(text + '<span><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 14 8" width="14" height="8"><path fill="#ffffff" fill-rule="evenodd" d="M1.47237.233812L6.99208 5.78645 12.5423.233812c.7319-.750357 1.9822.480228 1.2198 1.230588l-6.25159 6.333c-.27446.27013-.73189.27013-.97586 0L.222053 1.4644C-.509842.71404.740479-.516545 1.47237.233812z" clip-rule="evenodd"/></svg></span>')
    })
  }

  $('.s-main-nav__list > li > a > span').on('click', function (e) {
    e.preventDefault();
    $(this).toggleClass('is-active');
    $(this).parent().next('ul').toggleClass('is-open');
  })

  $(window).resize(function () {
    if ($('.s-main-nav__list').css('flex-direction') == 'column') {

      $('.s-main-nav__list > li > a').each(function () {
        var text = $(this).text()
        $(this).html(text + '<span><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 14 8" width="14" height="8"><path fill="#ffffff" fill-rule="evenodd" d="M1.47237.233812L6.99208 5.78645 12.5423.233812c.7319-.750357 1.9822.480228 1.2198 1.230588l-6.25159 6.333c-.27446.27013-.73189.27013-.97586 0L.222053 1.4644C-.509842.71404.740479-.516545 1.47237.233812z" clip-rule="evenodd"/></svg></span>')
      })

      $('.s-main-nav__list > li > a > span').on('click', function (e) {
        e.preventDefault();
        $(this).toggleClass('is-active');
        $(this).parent().next('ul').toggleClass('is-open');
      })
    }
  });

  $('.s-rubrics__list > li > a').each(function () {
    var text = $(this).text()
    $(this).html(text + '<span><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 14 8" width="14" height="8"><path fill="#414141" fill-rule="evenodd" d="M1.47237.233812L6.99208 5.78645 12.5423.233812c.7319-.750357 1.9822.480228 1.2198 1.230588l-6.25159 6.333c-.27446.27013-.73189.27013-.97586 0L.222053 1.4644C-.509842.71404.740479-.516545 1.47237.233812z" clip-rule="evenodd"/></svg></span>')
  })

  $('.s-rubrics__list > li > a > span').on('click', function (e) {
    e.preventDefault();
    $(this).toggleClass('is-active');
    $(this).parent().next('ul').toggleClass('is-open');
  })

  function resizeScrenn() {
    if ($(window).width() <= 767) {
      if ($('.s-inner-layout__poll .swiper-container').length > 0) {
        swiper = new Swiper('.s-inner-layout__poll .swiper-container', {
          slidesPerView: 1
        });
      }
    } else {
      swiper.destroy();
    }
  }

  resizeScrenn();

  $(window).resize(function () {
    resizeScrenn();
  });


});